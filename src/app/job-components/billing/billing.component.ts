import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

interface County {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.css']
})
export class BillingComponent implements OnInit {
  frmStepTwo: FormGroup;
  selectedValue: string | undefined;
  counties: County[] = [
    {value: 'Athens-Clarke County', viewValue: 'Athens-Clarke'},
    {value: 'Augusta-Richmond County', viewValue: 'Augusta-Richmond'},
    {value: 'Columbus-Muscogee County', viewValue: 'Columbus-Muscogee'}
  ];
  constructor(private formBuilder: FormBuilder) {
    this.frmStepTwo = this.formBuilder.group({
      contactName: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      email: ['', Validators.required],
      // sameAsCompanyAddress: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      zipCode: ['', Validators.required],
      county: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

}
