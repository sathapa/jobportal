import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

interface County {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {

  frmStepOne: FormGroup;
  selectedValue: string | undefined;
  counties: County[] = [
    {value: 'Athens-Clarke County', viewValue: 'Athens-Clarke'},
    {value: 'Augusta-Richmond County', viewValue: 'Augusta-Richmond'},
    {value: 'Columbus-Muscogee County', viewValue: 'Columbus-Muscogee'}
  ];

  constructor(private formBuilder: FormBuilder) {

    this.frmStepOne = this.formBuilder.group({
      stateDepartment: ['', Validators.required],
      stateDivision: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      zipCode: ['', Validators.required],
      county: ['', Validators.required],
    });
  }

  ngOnInit(): void {
  }

}
