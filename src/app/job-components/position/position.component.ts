import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.css']
})
export class PositionComponent implements OnInit {
  frmStepThree: FormGroup;

  constructor(private formBuilder: FormBuilder) { 
    this.frmStepThree = this.formBuilder.group({
      positionTitle: ['', Validators.required],
      sameAsCompanyAddress: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      zipCode: ['', Validators.required],
      county: ['', Validators.required],
      hourlyWage: ['', Validators.required],
      hoursWorked: ['', Validators.required],
      shiftStart: ['', Validators.required],
      shiftEnd: ['', Validators.required],
      schedule: ['', Validators.required],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      jobDescription: ['', Validators.required],
    });
  }

  ngOnInit(): void {
  }

}
