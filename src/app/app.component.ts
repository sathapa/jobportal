import { Component, ViewChild, AfterViewInit, ChangeDetectorRef, OnInit } from '@angular/core';
import { CompanyComponent } from './job-components/company/company.component';
import { SupervisorComponent } from './job-components/supervisor/supervisor.component';
import { BillingComponent } from './job-components/billing/billing.component';
import { PositionComponent } from './job-components/position/position.component';

import { JobService } from './services/job.service';

import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  title = 'mat-stepper';
  submitted = false; 
  
  form1!: FormGroup;
  form2!: FormGroup;
  form3!: FormGroup;
  form4!: FormGroup;
  @ViewChild('stepOne') stepOneComponent!: CompanyComponent;
  @ViewChild('stepTwo') stepTwoComponent!: BillingComponent;
  @ViewChild('stepThree') stepThreeComponent!: PositionComponent;
  @ViewChild('stepFour') stepFourComponent!: SupervisorComponent;

  constructor( private cdr :ChangeDetectorRef, private JobService: JobService ){}

  ngAfterViewInit(){
    this.form1 = this.stepOneComponent.frmStepOne;
    this.form2 = this.stepTwoComponent.frmStepTwo;
    this.form3 = this.stepThreeComponent.frmStepThree;
    this.form4 = this.stepFourComponent.frmStepFour;
    this.cdr.detectChanges();
  }
  
  saveJob() {
    // console.log( this.form1.value );
    // console.log( this.form2.value );
    // console.log( this.form3.value );
    // console.log( this.form4.value );  
    
    const data = {
      company : this.form1.value,
      billing: this.form2.value,
      position: this.form3.value,
      supervisor: this.form4.value
    };

    console.log( data );

    this.JobService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        }, 
        error => {
          console.log(error);
        }
      );
  
  }

}
