# jobPortal

Angular UI which uses multi-step nested Reactive forms.

#Steps to Run locally
1. Check if npm is installed by doing npm -v. If npm not install then npm needs to be installed. follow this [link](https://www.npmjs.com/get-npm). 
Now install Angular CLI on terminal/Command prompt - 
npm install -g @angular/cli 

2. Go to source folder and run npm install.
3. Once all the packages are installed, run ng serve to execute.
4. Go to the local browser and type http://localhost:4200.

